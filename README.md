# Infoglobo Challenge

A React CRUD application for Infoglobo.
 
## Setup and Run (Step-by-Step)

1. Setup and run the back-end application (Steps described on README.md inside "back-end" folder).
2. Go to "front-end" folder (infoglobo-challenge/front-end) and run the following commands:
    ```
    npm install
    ```
3. After successful installation, run project by executing:
    ```
    npm start
    ```