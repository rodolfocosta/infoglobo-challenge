import React, { Component } from 'react';

export default class NewsItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEdit : false
    }
    this.editRow = this.editRow.bind(this);
    this.updateTableNews = this.updateTableNews.bind(this);
    this.deleteRow = this.deleteRow.bind(this);
  }
  deleteRow = () => {
    this.props.deleteNews(this.props.news._id.$oid);
  }
  editRow = () => {
    this.setState((prevState) => ({
      isEdit : !prevState.isEdit
    }))
  }
  updateTableNews = () => {
    this.setState((prevState) => ({
      isEdit : !prevState.isEdit
    }));
     
    this.props.updateNews(
      this.props.news._id.$oid,
      this.titleInput.value,
      this.contentInput.value,
      this.publicationDateInput.value
    );
  }
  render = () => {
    const {title,content,publication_date} = this.props.news;
    return (
      this.state.isEdit === true ? (
        <tr className="bg-warning" key={this.props.index}>
          <td>
            <input defaultValue={title} ref={titleInput => this.titleInput = titleInput}/>
          </td>
          <td>
            <input defaultValue={content} ref={contentInput => this.contentInput = contentInput}/>
          </td>
          <td>
            <input defaultValue={publication_date} ref={publicationDateInput => this.publicationDateInput = publicationDateInput}/>
          </td>
          <td><button className="tableButton greenButton" onClick={this.updateTableNews}>Atualizar</button></td>
          <td><button className="tableButton redButton" onClick={this.deleteRow}>Remover</button></td>
        </tr>
      ) : (
        <tr key={this.props.index}>
          <td>{title}</td>
          <td>{content}</td>
          <td>{publication_date}</td>
          <td><button className="tableButton greenButton" onClick={this.editRow}>Editar</button></td>
          <td><button className="tableButton redButton" onClick={this.deleteRow}>Remover</button></td>
        </tr>
      )
    );
  }
}