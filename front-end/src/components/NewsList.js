import React, { Fragment } from 'react';

import './style.css';
import NewsItem from './NewsItem';

export default class NewsList extends React.Component {
  render = () => {
    return (
      <Fragment>
        { this.props.newsList.map((news, key) => 
          <NewsItem
            key={key}
            news={news}
            index={key}
            updateNews={this.props.updateNews}   
            deleteNews={this.props.deleteNews}
          />
          )
        }
      </Fragment>
    )
  }
}