import React, { Component } from 'react';
import axios from 'axios';

import './style.css';

export default class NewsCreator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      content: "",
      publication_date: ""
    };
  }
  handleChange = (event) => {
    this.setState({[event.target.name]: event.target.value});
  }
  handleSubmit = (event) => {    
    axios.post(`http://localhost:5000/news`, this.state)
      .then(res => {
        axios.get(`http://localhost:5000/news?title=`+this.state.title)
          .then(res => {
            const createdNews = res.data[0];
            this.props.createNews(createdNews);
          })
          .catch((error) => {
            console.log(error);
          })
      })
      .catch((error) => {
        console.log(error);
      })

    event.preventDefault();
  }
  render = () => {
    return (
      <form onSubmit={this.handleSubmit}>
        <label className="display-block">
          Título:
          <input className="display-block" type="text" value={this.state.value} name="title" onChange={this.handleChange} />
        </label>
        <label className="display-block">
          Conteúdo:
          <input className="display-block" type="text" value={this.state.value} name="content" onChange={this.handleChange} />
        </label>
        <label className="display-block">
          Data de publicação:
          <input className="display-block" type="text" value={this.state.value} name="publication_date" onChange={this.handleChange} />
        </label>
        <input type="submit" value="Criar" />
      </form>
    );
  }
}