import React, { Component } from 'react';
import axios from 'axios';

import './App.css';
import NewsList from "./components/NewsList";
import NewsCreator from "./components/NewsCreator";

// if (localStorage.getItem("news") === null) {
//   localStorage.setItem("localNewsList", JSON.stringify(newsList));
// }

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCreatorVisible : false,
      newsList: []
    }
    this.deleteNews = this.deleteNews.bind(this);
    this.createNews = this.createNews.bind(this);
  }
  componentWillMount = () => {
    axios.get(`http://localhost:5000/news`)
      .then(res => {
        const newsJson = res.data;
        this.setState({ newsList: newsJson });
      })
      .catch((error) => {
        console.log(error);
      })
  }
  updateTableRow = (id, title, content, publication_date) => {
    let newsListCopy = this.state.newsList.map((news) => {  
      if (news._id.$oid === id) {
        news.title = title;
        news.content = content;
        news.publication_date = publication_date;
      }
      return news;
    });
    this.setState(() => ({
      newsList: newsListCopy
    }));
    localStorage.setItem(
      "localNewsList",
      JSON.stringify(newsListCopy)
    );
  }
  updateTableAndApi = (id, title, content, publication_date) => {
    const newsObject = {title, content, publication_date}

    axios.put(`http://localhost:5000/news/`+id, newsObject)
        .then(res => {
          this.updateTableRow(id, title, content, publication_date);
        })
        .catch((error) => {
          console.log(error); //Logs a string: Error: Request failed with status code 404
        })
  }
  deleteTableRow = (id) => {
    let filteredNewsList = this.state.newsList.filter(
      news => news._id.$oid !== id
    );
     this.setState((prevState, props) => ({
        newsList: filteredNewsList
     }));
     localStorage.setItem(
       "localNewsList",
       JSON.stringify(filteredNewsList)
     );
  }
  deleteNews = (id) => {
    let response = window.confirm("Deseja deletar essa notícia?");
    if (response) {
      axios.delete(`http://localhost:5000/news/`+id)
        .then(res => {
          this.deleteTableRow(id);
        })
        .catch((error) => {
          console.log(error); //Logs a string: Error: Request failed with status code 404
        })
    }
  }
  viewCreator = () => {
    this.setState((prevState) => ({
      isCreatorVisible : !prevState.isCreatorVisible
    }))
  }
  createNews = (createdNews) => {
    this.setState((prevState) => ({
      newsList: [...prevState.newsList, createdNews]
    }));

    this.setState((prevState) => ({
      isCreatorVisible : !prevState.isCreatorVisible
    }))
  }
  render = () => {
    return (
      <div className="container">
        <div className="row mt-3">
          <div className="col-lg-12">
            <div className="card">
              <div className="card-header">
                Infoglobo News
              </div>
              <div className="card-body">
                <table className="table table-hover">
                  <thead className="thead-dark">
                    <tr>
                      <th>Título</th>
                      <th>Conteúdo</th>
                      <th>Data de Publicação</th>
                      <th>Editar</th>
                      <th>Deletar</th>
                    </tr>
                  </thead>
                  <tbody>
                    <NewsList
                      newsList={this.state.newsList}
                      updateNews={this.updateTableAndApi}
                      deleteNews={this.deleteNews}
                    />
                  </tbody>
                </table>
              </div>
            </div>
            <div className="row">
              <div className="col ml-3 mt-3">
                <button
                  className="btn btn-dark pull-left"
                  onClick={this.viewCreator}>
                  Criar Notícia
                </button>
              </div>
            </div>
            { this.state.isCreatorVisible ? (
              <div className="row mt-3">
                <div className="col ml-3 mt-3">
                  <NewsCreator 
                    createNews={this.createNews}
                  />
                </div>
              </div>
            ) : null }
          </div>
        </div>
      </div>
   );
  }
}
export default App;