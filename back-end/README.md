# A News CRUD Challenge - Infoglobo - Back-end

A RESTful News API for Infoglobo.

This API stores News informations such as Title, Content and Publication Date on a MongoDB database.

## Tools

This API was made using the following tools and technologies:

 * [Python 3.8.1](https://www.python.org/)
 * [PyMongo 3.11.0](https://pymongo.readthedocs.io/en/stable/)
 * [Flask 1.1.2](http://flask.pocoo.org/)
 * [Flask BasicAuth 1.1.1](https://flask-basicauth.readthedocs.io/en/latest/)
 * [Flask CORS 3.0.7](https://flask-cors.readthedocs.io/en/3.0.7/)
 * [MongoDB 4.0.11](http://docs.mongodb.org/manual/installation/)

## API Functionalities

* ### GET /news
    * **Description:** Retrieves all news from database or filter news based on title, content or publication_date. 
    * **Response Code:** 200 (OK).  
    * **Response Data:** Empty array (Empty database) or JSON containing a news list (Stored news). 
Ex: 
    ```
    [
        {
            "title": "Titulo 1",
            "content": "Conteudo 1",
            "publication_date": "04/08/2020",
            "_id": {
                "$oid": "5ba2afa9b07fa61a7c6ba2d0"
            }
        }
    ]
    ```    

* ### POST /news
    * **Description:** Inserts a news into database with a title, content and publication_date.  
    * **Request body:** JSON containing news informations (title, content and publication_date).  
Ex: 
    ```
    {
      "title": "Titulo 1",
      "content": "Conteudo 1",
      "publication_date": "04/08/2020"
    }
    ``` 
    * **Response Codes:** 201 (Created) - Successfull insert, 400 (Bad request) - One of the news informations is missing, 409 (Conflict) - News title already exists.  
    * **Response Data:** No JSON.  

* ### PUT /news/<news_id>
    * **Description:** Updates a news from database based on given *news_id*.  
    * **Request Parameter:** *news_id* (String).  
    * **Response Codes:** 204 (No Content) - Successfull update, 400 (Bad request) - Invalid *news_id* or one of the news informations is missing.   
    * **Response Data:** No JSON.

* ### DELETE /news/<news_id>
    * **Description:** Deletes a news from database based on given *news_id*.  
    * **Request Parameter:** *news_id* (String).  
    * **Response Codes:** 204 (No Content) - Successfull deletion, 400 (Bad request) - Invalid *news_id*.
    * **Response Data:** No JSON.
 
## Setup and Run (Step-by-Step)

1. Install Python 3.8.1 or latest
2. Install [MongoDB](http://docs.mongodb.org/manual/administration/install-community/) and initialize its service.
3. Open **cmd** (Windows) or **terminal** (Linux and Mac) and go to this project root folder (infoglobo-challenge/back-end)
4. Create a **virtualenv** for this project:
    ```
    python -m venv venv
    ```
5. Enter **virtualenv** by running following command:
    1. **Windows**: 
    ```
    venv\Scripts\activate
    ```
    2. **Linux and Mac**: 
    ```
    source venv/bin/activate
    ```
6. Install required python packages **(under activated virtualenv and on project root folder (infoglobo-challenge/back-end))**  :
    ```
    pip install -r requirements.txt
    ```
7. Run flask application:
    1. **Windows**:     
        1. `set FLASK_APP=newsapp`
        2. `set FLASK_ENV=development`
        3. `flask run`
    2. **Linux and Mac**:
        1. `export FLASK_APP=newsapp`
        2. `export FLASK_ENV=development`
        3. `flask run`

## Installing and Testing (Step-by-Step)

1. Open **cmd** (Windows) or **terminal** (Linux and Mac) and go to this project root folder (infoglobo-challenge/back-end)
2. Enter **virtualenv** and run:
    ```
    pip install -e .
    ```
3. Go to directory "tests" under this project root folder (infoglobo-challenge/back-end)
4. Run:
    ```
    python test_newsapp.py -v
    ```

