from newsapp import create_app
import unittest
import json

from bson import json_util

NEWS_URI = '/news'

class NewsAppTester(unittest.TestCase):

    def get_app_client(self):
        return create_app().test_client(self)

    def test_not_found_url_access(self):
        tester = self.get_app_client()
        response = tester.get('/', content_type='application/json')
        self.assertEqual(response.data, b'["Page not found"]')
        self.assertEqual(response.status_code, 404)

    def test_get_all_news(self):
        tester = self.get_app_client()
        response = tester.get(NEWS_URI, content_type='application/json')
        self.assertEqual(response.data, b'[]') # returns True if database is empty or unmatched search criteria
        self.assertEqual(response.status_code, 200)

    def test_create_news(self):
        tester = self.get_app_client()
        response = tester.post(NEWS_URI,
                               data=json.dumps(dict(title="Urgente!", content="Teste relampago")),
                               content_type='application/json')
        self.assertEqual(response.data, b'["Please set a publication_date for this news"]')
        self.assertEqual(response.status_code, 400)

    def test_update_news(self):
        tester = self.get_app_client()
        response = tester.delete(NEWS_URI+'/123', content_type='application/json')
        self.assertEqual(response.data, b'["Invalid id"]') # returns 400 if id is invalid for a MongoDB ObjectId
        self.assertEqual(response.status_code, 400)
    
    def test_delete_news(self):
        tester = self.get_app_client()
        response = tester.delete(NEWS_URI+'/5f286ef3afe9d2a7a3282b81', content_type='application/json') 
        self.assertEqual(response.data, b'["News with given id not found"]') # returns 404 if id does not exist on database
        self.assertEqual(response.status_code, 404)

if __name__ == '__main__':
    unittest.main()
