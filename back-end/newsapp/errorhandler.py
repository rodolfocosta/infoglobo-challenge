from flask import Response

from bson import json_util

def unauthorized(error):
    return Response(
        json_util.dumps(["Unauthorized"]),
        mimetype='application/json'
    ), 401

def forbidden_access(error):
    return Response(
        json_util.dumps(["Forbidden access"]),
        mimetype='application/json'
    ), 403

def page_not_found(error):
    return Response(
        json_util.dumps(["Page not found"]),
        mimetype='application/json'
    ), 404

def content_gone(error):
    return Response(
        json_util.dumps(["Content is gone"]),
        mimetype='application/json'
    ), 410

def internal_server_error(error):
    return Response(
        json_util.dumps(["Internal server error"]),
        mimetype='application/json'
    ), 500
