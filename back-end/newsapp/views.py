import json
import requests

from flask import (
    Blueprint, current_app, Response, abort, request, url_for
)

from bson import json_util
from bson.objectid import ObjectId
import bson

from werkzeug.exceptions import BadRequest

NEWS_URI = '/news'
JSON_MIMETYPE = 'application/json'
TITLE = "title"
CONTENT = "content"
PUBLICATION_DATE = "publication_date"

blueprint = Blueprint('newscrud', __name__, url_prefix='/')

@blueprint.route(NEWS_URI, methods=['GET'])
def get_all_news():
    """Retrieves all news from database."""
    title = request.args.get(TITLE)
    content = request.args.get(CONTENT)
    publication_date = request.args.get(PUBLICATION_DATE)

    news_collection = get_news_collection()
    news_documents = news_collection.find(get_find_query(title, content, publication_date))

    json_response = []
    for document in news_documents:
        json_response.append(document)

    return Response(
        json_util.dumps(json_response),
        mimetype=JSON_MIMETYPE
    ), 200

@blueprint.route(NEWS_URI, methods=['POST'])
def create_news(title="", content="", publication_date=""):
    """Inserts a news into database with a title, content and publication_date."""
    title = ""
    content = ""
    publication_date = ""
    request_json = ""

    try:
        request_json = request.get_json()
    except Exception as error:
        return bad_request("Invalid json format")

    if TITLE in request_json:
        title = request_json[TITLE]
    else:
        return bad_request("Please set a title for this news")

    if CONTENT in request_json:
        content = request_json[CONTENT]
    else:
        return bad_request("Please set a content for this news")

    if PUBLICATION_DATE in request_json:
        publication_date = request_json[PUBLICATION_DATE]
    else:
        return bad_request("Please set a publication_date for this news")

    # Insert a new document on MongoDB with our parameters values
    news_object = {TITLE: title,
                   CONTENT: content,
                   PUBLICATION_DATE: publication_date}

    try:
        news_collection = get_news_collection()
        news_collection.insert_one(news_object)
    except Exception as error:
        return Response(
            json_util.dumps(["News '"+title+"' already exists"]),
            mimetype=JSON_MIMETYPE
        ), 409

    return Response(status=201)

@blueprint.route(NEWS_URI+'/<news_id>', methods=['PUT'])
def update_news(news_id):
    """Updates a news from database based on given news_id."""
    object_id = ""
    title = ""
    content = ""
    publication_date = ""
    request_json = ""

    # Check if news_id is a 12-byte input or a 24-character hex string
    try:
        object_id = ObjectId(news_id)
    except bson.errors.InvalidId as error:
        return bad_request("Invalid id")

    try:
        request_json = request.get_json()
    except Exception as error:
        return bad_request("Invalid json format")

    if TITLE in request_json:
        title = request_json[TITLE]
    else:
        return bad_request("Please set a title for this news")

    if CONTENT in request_json:
        content = request_json[CONTENT]
    else:
        return bad_request("Please set a content for this news")

    if PUBLICATION_DATE in request_json:
        publication_date = request_json[PUBLICATION_DATE]
    else:
        return bad_request("Please set a publication_date for this news")

    news_object = {TITLE: title,
                   CONTENT: content,
                   PUBLICATION_DATE: publication_date}

    news_collection = get_news_collection()
    find_query = {'_id': object_id}
    new_values = { "$set": news_object }

    update_count = news_collection.update_one(find_query, new_values)
    if update_count.matched_count == 0:
        return not_found("News with given id not found")

    return Response(status=204)

@blueprint.route(NEWS_URI+'/<news_id>', methods=['DELETE'])
def delete_news(news_id):
    """Deletes a news from database based on given news_id."""
    object_id = ""

    # Check if news_id is a 12-byte input or a 24-character hex string
    try:
        object_id = ObjectId(news_id)
    except bson.errors.InvalidId as error:
        return bad_request("Invalid id")

    news_collection = get_news_collection()
    delete_result = news_collection.delete_one({'_id': object_id})

    if delete_result.deleted_count == 0:
        return not_found("News with given id not found")

    return Response(status=204)

def get_news_collection():
    """Retrieve a cursor for news collection from database."""
    return current_app.mongo.db["news"]

def get_find_query(title, content, publication_date):
    """Returns a query to find news on database."""
    find_query = {}

    if title:
        find_query = {TITLE: title}
    if content:
        find_query = {CONTENT: content}
    if publication_date:
        find_query = {PUBLICATION_DATE: publication_date}
    if title and content:
        find_query = {TITLE: title, CONTENT: content}
    if title and publication_date:
        find_query = {TITLE: title, PUBLICATION_DATE: publication_date}
    if content and publication_date:
        find_query = {CONTENT: content, PUBLICATION_DATE: publication_date}
    if title and content and publication_date:
        find_query = {TITLE: title, CONTENT: content, PUBLICATION_DATE: publication_date}

    return find_query

def bad_request(message):
    return Response(
            json_util.dumps([message]),
            mimetype=JSON_MIMETYPE
        ), 400

def not_found(message):
    return Response(
            json_util.dumps([message]),
            mimetype=JSON_MIMETYPE
        ), 404
