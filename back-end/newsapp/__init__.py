import os

from flask import Flask
from flask_pymongo import PyMongo
from flask_basicauth import BasicAuth
from flask_cors import CORS

from . import views
from . import errorhandler

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # configuring Basic Auth
    # app.config['BASIC_AUTH_USERNAME'] = 'infoglobo'
    # app.config['BASIC_AUTH_PASSWORD'] = 'challenge'
    # app.config['BASIC_AUTH_FORCE'] = True
    # basic_auth = BasicAuth(app)

    # configuring MongoDB
    app.config["MONGO_URI"] = "mongodb://localhost:27017/newsappDB"
    app.mongo = PyMongo(app)

    # enabling CORS
    CORS(app)

    # creating unique index for 'title' property
    app.mongo.db["news"].create_index("title", unique=True)

    # all views (api + database)
    app.register_blueprint(views.blueprint)

    # common errors handling
    app.register_error_handler(401, errorhandler.unauthorized)
    app.register_error_handler(403, errorhandler.forbidden_access)
    app.register_error_handler(404, errorhandler.page_not_found)
    app.register_error_handler(410, errorhandler.content_gone)
    app.register_error_handler(500, errorhandler.internal_server_error)

    return app
